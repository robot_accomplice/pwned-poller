**Pwned Poller** is a Java-based poller which can be run ad hoc or as a service/daemon (using [Apache Commons-Daemon](https://commons.apache.org/proper/commons-daemon/) and procrun.exe or jsvc) to check for the presence of compromised user supplied emails and 
domain names on [Have I been Pwned](http://haveibeenpwned.com).  It is driven by configuration files (JSON, XML, and YAML are supported) and can optionally 
persist the results to either an in memory or on disk database.

All configuration options (save operating mode) are specified in one of two configuration files (initially XML was chosen for its ubiquity, simplicity, and consistent structure, but support for JSON and YAML have since been added):  

*  Program specific configuration containing only those elements which are particular to the solution
*  The configuration file for Apache Log4J (which, in addition to providing console or disk-based logging, also provides the support for syslog messages).  Links to supporting documentation for both LOG4J and Apache Commons Daemon (the mechanism by which service/daemon support is achieved) are included in the external documentation section of this report.  A more detailed description of the design decisions made by the developer is included in the next section.

**DESIGN DECISIONS**

**Datastore Technology**

Although several datastores were evaluated, OrientDB was ultimately selected for the reasons below.
* Open source - Reducing initial costs with an option to upgrade to Enterprise for full 24/7 support and additional functionality.
* Multiple execution/hosting models - Can be run as an in memory, local embedded, or “remote” (separately managed) datastore. 
* Multiple data models - Can be used as a document, graph, or object store providing the maximum level of flexibility for future feature upgrades
* SQL support - For simplified querying in a familiar language

**Operating Modes**

*  Ad hoc execution mode - Suitable for testing or “cronjob” scheduling: 
> java -Drun_once=true -Dconfig.file=<path to config xml file> -Dlog4j.configurationFile=<path to log4j configuraiton xml> -jar <jar name> 
*  Continuous (daily) execution mode - Time of day is externally configured (via the configuration XML file) and internally controlled
*  Windows service / Linux daemon mode - Ideal for ordered and “clean” shutdown (particularly relevant if a non-embedded datastore is used).  

Links to detailed documentation are provided in the “EXTERNAL DOCUMENTATION LINKS” section.

**Programming Language**

Java was selected as a function of its familiarity to the developer and its platform independence.

**Parallelization**

While a degree of parallelization is achievable, per the API documentation (and my own tests) there is (wisely) a single call per 1500 ms rate limit (documented [here](https://haveibeenpwned.com/API/v2)) per API call type (which can be loosely described as site breach, pastebin data, and account breach), per initiating IP address.  Subsequently, the developer was left with the following options:
*  Do not support parallelization
*  Limit parallel calls to separate API invocations (the option chosen by the author)

**Limitations**

While the embedded database mode (plocal) eliminates an initial need for external dependencies, the embedded database cannot be queried while the application is running.  Future versions of the application could be modified to incorporate external DB instrumentation, but this initial version of the application forgoes this functionality to limit project scope and avoid the security implications of exposing TCP services from a non-managed DB instance.  It is therefore recommended that a standalone instance of OrientDB (with its console and browser-based DB instrumentation) be installed and configured to host the target database (either locally or remotely) so that queries can be executed without suspending application execution.

**EXTERNAL DOCUMENTATION LINKS**

*  [Apache Log4J Configuration](https://logging.apache.org/log4j/2.x/manual/configuration.html)
*  [OrientDB](http://orientdb.com/docs/last/index.html)
*  Apache Commons Daemon
   *  [Windows Service (procrun)](https://commons.apache.org/proper/commons-daemon/procrun.html)
   *  [Linux Daemon (jsvc)](https://commons.apache.org/proper/commons-daemon/jsvc.html)

**CONFIGURATION EXAMPLES**

**Example application configuration XML file:**

```
<?xml version="1.0" encoding="UTF-8"?>
<config frequency="DAILY" startTime="18:55" >
    <!-- 
        DB config - the nature of dbpath (remote/plocal) will determine 
        whether DB is hosted by a separate server (local OR remote) or hosted as 
        an embedded and persistent (plocal) instance.
        A plocal instance cannot be queried while this application is running, but
        requires a smaller footprint and allows external OrientDB dependencies to 
        be managed independently and at a later time
    -->
    <dbPath>plocal:C:\orientdb-community-importers-2.2.31\databases\Pwned-Poller</dbPath>
    <dbUser>admin</dbUser>
    <dbPass>admin</dbPass>
    
    <!-- sites to track -->
    <sites>
        <site>evilcorp.com</site>
        <site>evilcorp.net</site>
        <site>mrrobot.com</site>
        <site>thinkgeek.com</site>
    </sites>
    
    <!-- accounts to track -->
    <accounts>
        <account>philip.price@evilcorp.com</account>
        <account>support@thinkgeek.com</account>
        <account>nobody@all.ru</account>
    </accounts>
</config>
```

**Example Log4j configuration:**

```
<Configuration monitorInterval="30" status="warn">
    <!-- Define custom SYSLOG level before using it for filtering below -->
    <CustomLevels>
        <CustomLevel name="SYSLOG" intLevel="50" />
    </CustomLevels>        
    <Appenders>
        <Console name="stdout" target="SYSTEM_OUT">
            <PatternLayout pattern="%5p [%d] %F (%L) : %m%n" />
        </Console>
	<!-- host, port, protocol, and appName attributes can be tailored to installation -->
        <Syslog name="SYSLOG" host="localhost" port="514" protocol="UDP" appName="pwned"
            facility="SYSLOG">
            <ThresholdFilter level="SYSLOG" onMatch="ACCEPT" onMismatch="DENY"/>            
        </Syslog>
    </Appenders>
    <Loggers>
        <Logger name="com.crowdstrike.evilcorp.Pwned" level="SYSLOG" additivity="false">
            <AppenderRef ref="SYSLOG"/>
        </Logger>
        
        <!-- this is necessary because of some quirky behavior exhibited by log4j -->
        <Logger name="com.robotaccomplice.pwned.Pwned" level="info" additivity="false">
            <AppenderRef ref="stdout"/>
        </Logger>
        
        <Root level="info">
            <AppenderRef ref="stdout"/>
        </Root>
    </Loggers>
</Configuration>
```
