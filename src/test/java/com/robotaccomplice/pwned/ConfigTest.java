/*
 * Copyright (C) 2017 Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.robotaccomplice.pwned;

import com.robotaccomplice.configamajig.ConfigException;
import com.robotaccomplice.configamajig.Configamajig;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public class ConfigTest {

    static Config configXML;
    static Config configYML;
    static Config configJSON;

    /**
     * 
     */
    public ConfigTest() {}

    /**
     * 
     */
    @BeforeClass
    public static void setUpClass() {
        try {
            configXML = Configamajig.fromXmlFile(Config.class, "src/main/resources/sample_config.xml");
        } catch (ConfigException ex) {
            Logger.getLogger(ConfigTest.class.getName()).log(Level.SEVERE, "Config retrieval failed!", ex);
        }
        try {
            configYML = Configamajig.fromYamlFile(Config.class, "src/main/resources/sample_config.yml");
        } catch (ConfigException ex) {
            Logger.getLogger(ConfigTest.class.getName()).log(Level.SEVERE, "Config retrieval failed!", ex);
        }
        try {
            configJSON= Configamajig.fromJsonFile(Config.class, "src/main/resources/sample_config.json");
        } catch (ConfigException ex) {
            Logger.getLogger(ConfigTest.class.getName()).log(Level.SEVERE, "Config retrieval failed!", ex);
            ex.printStackTrace(System.out);
        }
    }

    /**
     * Test of getStartTime method, of class Config.
     */
    @Test
    public void testGetStartTime() {
        System.out.println("Testing: Config.getStartTime()");

        ZonedDateTime expResult = ZonedDateTime.of(LocalDate.now(), LocalTime.parse("10:00"), ZoneOffset.systemDefault());

        if (expResult.isBefore(ZonedDateTime.now())) expResult = expResult.plusDays(1);

        try {
            ZonedDateTime result = configXML.getStartTime();
            assertEquals("XML Config failed time parse ", expResult, result);
            assertEquals("YML Config failed time parse ", result, configYML.getStartTime());
            assertEquals("JSON Config failed time parse ", result, configJSON.getStartTime());
            System.out.println("getStartTime test successful with a returned result of: " + result);
        } catch (ConfigException ce) {
            ce.printStackTrace(System.out);
            fail("Config could not be parsed: " + ce.getMessage());
        }
    }

    /**
     * Test of getSites method, of class Config.
     */
    @Test
    public void testGetSites() {
        System.out.println("Testing: Config.getSites()");
        List<String> expResult = new ArrayList<>();
        expResult.add("evilcorp.com");
        expResult.add("evilcorp.net");
        expResult.add("mrrobot.com");
        expResult.add("thinkgeek.com");
        expResult.add("armyforceonline.com");

        List<String> result = configXML.getSites();
        assertEquals("XML Config failed sites check ", expResult, result);
        assertEquals("YML Config failed sites check ", result, configYML.getSites());
        assertEquals("JSON Config failed sites check ", result, configJSON.getSites());
        System.out.println("getSites test successful with a returned result of: " + result);
    }

    /**
     * Test of getAccounts method, of class Config.
     */
    @Test
    public void testGetAccounts() {
        System.out.println("Testing: Config.getAccounts()");
        List<String> expResult = new ArrayList<>();
        expResult.add("philip.price@evilcorp.com");
        expResult.add("jonathan.machen@robotaccomplice.com");
        expResult.add("support@thinkgeek.com");
        expResult.add("nobody@all.ru");

        List<String> result = configXML.getAccounts();
        assertEquals("XML Config failed accounts check ", expResult, result);
        assertEquals("YML Config failed accounts check ", result, configYML.getAccounts());
        assertEquals("JSON Config failed accounts check ", result, configJSON.getAccounts());
        System.out.println("getAccounts test successful with a returned result of: " + result);
    }
    
    /**
     * 
     */
    @Test
    public void testGetFrequency() {
        System.out.println("Testing: Config.getFrequncy()");
        assertEquals("XML Config failed frequency check ", configXML.getFrequency(), Frequency.DAILY);
        assertEquals("YML Config failed frequency check ", configYML.getFrequency(), Frequency.DAILY);
        assertEquals("JSON Config failed frequency check ", configJSON.getFrequency(), Frequency.DAILY);
        System.out.println("getFrequency test successful with a returned result of " + configXML.getFrequency());
    }
}
