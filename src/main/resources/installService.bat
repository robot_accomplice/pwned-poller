REM ************************************
REM installService.bat
REM ************************************

set INSTALL_PATH=<path to location where Pwned Poller is installed>
set SERVICE_NAME=pwned-poller
set PR_EXE=%INSTALL_PATH%\bin\prunsrv.exe

REM Path to java installation
set PR_JVM=<path to JRE>\bin\client\jvm.dll
set PR_CLASSPATH=%INSTALL_PATH%\lib\pwned-poller-1.0-SNAPSHOT-jar-with-dependencies.jar
  
REM Service log configuration
set PR_LOGPREFIX=%SERVICE_NAME%
set PR_LOGPATH=%INSTALL_PATH%\log
set PR_STDOUTPUT=%PR_LOGPATH%\service_stdout.txt
set PR_STDERROR=%PR_LOGPATH%\service_stderr.txt
set PR_LOGLEVEL=Error
 
REM Startup configuration
set PR_STARTUP=auto
set PR_STARTMODE=jvm
set PR_STARTCLASS=com.crowdstrike.evilcorp.Pwned
set PR_STARTMETHOD=start
 
REM Shutdown configuration
set PR_STOPMODE=jvm
set PR_STOPCLASS=com.crowdstrike.evilcorp.Pwned
set PR_STOPMETHOD=stop
 
REM JVM configuration
set PR_JVMMS=256
set PR_JVMMX=1024
set PR_JVMSS=4000
set PR_JVMOPTIONS=-Dconfig.file=%INSTALL_PATH%\conf\sample_config.xml
set PR_JVMOPTIONS=-Dlog4j.configurationFile=%INSTALL_PATH%\conf\log4j2.xml

REM Install service
prunsrv.exe //IS//%SERVICE_NAME%