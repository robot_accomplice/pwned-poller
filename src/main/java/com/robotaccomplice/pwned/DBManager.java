/*
 * Copyright (C) 2017-2019 Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.robotaccomplice.pwned;

import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.exception.OCommandExecutionException;
import com.orientechnologies.orient.core.exception.ODatabaseException;
import com.orientechnologies.orient.core.exception.OStorageException;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Jonathan Machen {@literal <jonathan.machen@robotaccomplice.com>}
 */
public class DBManager {

    private final static Logger LOG = LogManager.getLogger(DBManager.class);

    private static OrientDB orientDB;
    private ODatabaseDocument db;
    private final String path;
    private final String user;
    private final String pass;
    
    // parameterized SQL statements
    private final static String QUERY_ACCOUNTS_BY_NAME = 
            "SELECT * FROM Account WHERE Name = :accountName";
    private final static String QUERY_PASTEBINS_BY_SOURCE_AND_ID =
            "SELECT * FROM Pastebin WHERE Source = :pasteBinSource AND Id = :pastebinId";
    private final static String QUERY_BREACHES_BY_NAME =
            "SELECT * FROM Breach WHERE Name = :breachName";
    private final static String EXEC_STORE_PASTEBIN =
            "UPDATE Breach SET Source = :source, Id = :id, Date = :date', EmailCount = :emailCount UPSERT WHERE (Source = :source AND Id = :id)";
    private final static String EXEC_STORE_BREACH = 
            "UPDATE Breach SET Name = :name, Title = :title, Domain = :domain, BreachDate = :breachDate, AddedDate = :addedDate, " +
            "ModifiedDate = :modifiedDate, PwnCount = :pwnCount, Description = :description, DataClasses = :dataClasses, " +
            "LogoPath = :logoPath, IsVerified = :isVerified, IsFabricated = :isFabricated, IsSensitive = :isSensitive, " +
            "IsRetired = :isRetired, IsSpamList = :isSpamList " +
            "UPSERT WHERE Title = :title";
    private final static String EXEC_STORE_ACCOUNT_BREACHES =
            "UPDATE Account SET Breaches = Breaches || :breachId WHERE Name = :accountName";
    private final static String EXEC_STORE_ACCOUNT_PASTEBINS =
            "UPDATE Account SET PastebinHits = PastebinHits || :pasteBins WHERE Name = :accountName";
    
    /**
     *
     * @param url
     * @param user
     * @param pass
     */
    public DBManager(String url, String user, String pass) {
        this.path = url;
        this.user = user;
        this.pass = pass;
    }

    /**
     *
     */
    public void initDB() throws ODatabaseException {
        if (orientDB == null) {
            LOG.info("Initializing DB at: {}", path);
            if (user == null) throw new NullPointerException("DB user is null");
            if (pass == null) throw new NullPointerException("DB pass is null");
            if (OrientDBConfig.defaultConfig() == null) 
                throw new NullPointerException("OrientDBConfig.defaultConfig() is null");
            orientDB = new OrientDB(path, user, pass, OrientDBConfig.defaultConfig());
            if(!orientDB.createIfNotExists("PwnedPoller", ODatabaseType.PLOCAL)) 
                LOG.debug("Existing Database found at: {}", path);
            LOG.debug("Opening DB...");
            db = orientDB.open(path, user, pass);
            LOG.debug("Activating DB on current thread...");
            db.activateOnCurrentThread();
            buildDb();
        } else {
            db.activateOnCurrentThread();
        }
    }

    /**
     *
     */
    public void buildDb() {
        LOG.info("Checking for existence of schema elements...");
        if (!schemaExists("Breach")) {
            LOG.info("Creating schema \"Breach\"");
            // create breach schema
            OClass breach = db.getMetadata().getSchema().createClass("Breach");

            breach.createProperty("Name", OType.STRING);
            breach.createProperty("Title", OType.STRING);
            breach.createProperty("Domain", OType.STRING);
            breach.createProperty("BreachDate", OType.DATE);
            breach.createProperty("AddedDate", OType.DATETIME);
            breach.createProperty("ModifiedDate", OType.DATETIME);
            breach.createProperty("PwnCount", OType.INTEGER);
            breach.createProperty("Description", OType.STRING);
            breach.createProperty("DataClasses", OType.STRING);
            breach.createProperty("LogoPath", OType.STRING);
            breach.createProperty("IsVerified", OType.BOOLEAN);
            breach.createProperty("IsFabricated", OType.BOOLEAN);
            breach.createProperty("IsSensitive", OType.BOOLEAN);
            breach.createProperty("IsRetired", OType.BOOLEAN);
            breach.createProperty("IsSpamList", OType.BOOLEAN);
            breach.createIndex("ix-u-Breach.Name", OClass.INDEX_TYPE.UNIQUE, "Name");
        }

        if (!schemaExists("Pastebin")) {
            LOG.info("Creating schema \"Pastebin\"...");
            // create pastebin schema
            OClass pastebin = db.getMetadata().getSchema().createClass("Pastebin");
            pastebin.createProperty("Source", OType.STRING);
            pastebin.createProperty("Id", OType.STRING);
            pastebin.createProperty("Title", OType.STRING);
            pastebin.createProperty("Date", OType.DATE);
            pastebin.createProperty("EmailCount", OType.INTEGER);
            pastebin.createIndex("ix-c-Pastebin.Source-Pastebin.Id", OClass.INDEX_TYPE.NOTUNIQUE, "Source", "Id");
        }

        if (!schemaExists("Account")) {
            LOG.info("Creating schema \"Account\"...");
            // create account schema
            OClass account = db.getMetadata().getSchema().createClass("Account");
            account.createProperty("Name", OType.STRING);
            account.createProperty("Breaches", OType.LINKSET, db.getMetadata().getSchema().getClass("Breach"));
            account.createProperty("PastebinHits", OType.LINKSET, db.getMetadata().getSchema().getClass("Pastebin"));
            account.createIndex("ix-u-Account.Name", OClass.INDEX_TYPE.UNIQUE, "Name");
        }
    }

    /**
     *
     * @param className
     * @return
     */
    private boolean schemaExists(String className) {
        return db.getMetadata().getSchema().existsClass(className);
    }

    /**
     *
     * @param rClass a String representing the name of the record class
     * @param data a JSONObject containing the data to store
     */
    private void storeRecordFromJson(String rClass, JSONObject data) {
        try {
            ODocument doc = new ODocument(rClass);
            doc.fromJSON(data.toString());
            doc.save();
        } catch (ORecordDuplicatedException orde) {
            LOG.warn("Record already exists - ORecordDuplicateException: {}", 
                    orde.getMessage(), orde);
        } catch (Exception e) {
            LOG.error("{} - Failed to store {} record with the following data:\n{}", 
                    e.getMessage(), rClass, data.toString(2), e);
        }
    }

    /**
     *
     * @param accountName
     */
    public void storeAccount(String accountName) {
        // check to see if record exists
        try (OResultSet accounts = 
                db.query(QUERY_ACCOUNTS_BY_NAME, Collections.singletonMap("accountName", accountName))) {
            if (!accounts.hasNext()) {
                LOG.info("Storing new Account record for account name \"{}\"", accountName);
                JSONObject data = new JSONObject();
                data.put("Name", accountName);
                data.put("Breaches", new JSONArray());
                data.put("PastebinHits", new JSONArray());
                storeRecordFromJson("Account", data);
            }
        }
    }

    /**
     *
     * @param accountName
     * @param breachName
     */
    public void addAccountBreach(String accountName, String breachName) {
        try(ODatabaseDocument odb = orientDB.open(path, user, pass)) {
            LOG.debug("Activating DB on current thread...");
            odb.activateOnCurrentThread();
            try (OResultSet accounts = 
                    odb.query(QUERY_ACCOUNTS_BY_NAME, Collections.singletonMap("accountName", accountName))) {
                if (accounts.hasNext()) {
                    Optional<OElement> record = accounts.next().getElement();
                    if (record.isPresent()) {
                        LOG.debug("Found the following breaches: {} ",
                                record.get().getProperty("Breaches").toString());

                        try ( // get breach record for Name
                                OResultSet breaches = odb.query(QUERY_BREACHES_BY_NAME, breachName)) {
                            if (!breaches.hasNext()) {
                                LOG.debug("There is no Breach called '{}' in the DB", breachName);
                            } else {
                                record = breaches.next().getElement();
                                if (record.isPresent()) {
                                    // add operations fail harmlessly and silently if the link is already present
                                    try {
                                        odb.execute("sql", EXEC_STORE_ACCOUNT_BREACHES,
                                                record.get().getIdentity(), accountName).close();
                                        LOG.info("Successfully added breach: '{}' to account '{}'", breachName, accountName);
                                    } catch(OCommandExecutionException | OStorageException oe) {
                                        LOG.error("There was an unforeseen problem with addAccountBreach query execution:"
                                                + "\nquery: {}"
                                                + "\nparams: {}, {}"
                                                + "\nerror: {}",
                                                EXEC_STORE_ACCOUNT_BREACHES,
                                                record.get().getIdentity(),
                                                accountName,
                                                oe.getMessage(), oe);
                                    }
                                }
                            }
                        }
                    } else {
                        LOG.debug("There is no Account record for Name '{}'", accountName);
                    }
                }
            }
        }
    }

    /**
     *
     * @param accountName
     * @param pastebinSource
     * @param pastebinId
     */
    public void addAccountPastebin(String accountName, String pastebinSource, String pastebinId) {
        try(ODatabaseDocument odb = orientDB.open(path, user, pass)) {
            LOG.debug("Activating DB on current thread...");
            odb.activateOnCurrentThread();
            try (OResultSet accounts = 
                    odb.query(QUERY_ACCOUNTS_BY_NAME, Collections.singletonMap("accountName", accountName))) {
                if (accounts.hasNext()) {
                    Optional<OElement> result = accounts.next().getElement();
                    if (result.get().getProperty("PasebinHits") != null) {
                        LOG.fatal("Found the following pastebin hits: {} ",
                                result.get().getProperty("PasebinHits").toString());
                    }

                    try (OResultSet pastebins = 
                            odb.query(QUERY_PASTEBINS_BY_SOURCE_AND_ID, pastebinSource, pastebinId)) {
                        if (!pastebins.hasNext()) {
                            LOG.debug("There is no Pastebin record for Source '{}' and Id '{}'",
                                    pastebinSource, pastebinId);
                        } else {
                            // add operations fail harmlessly and silently if the link is already present
                            odb.execute("sql", EXEC_STORE_ACCOUNT_PASTEBINS, 
                                    pastebins.next().getIdentity(), accountName).close();
                            LOG.info("Successfully added pastebin hit for Source:'{}' Id:{}' for account '{}'",
                                    pastebinSource, pastebinId, accountName);
                        }
                    }
                } else {
                    LOG.debug("There is no Account record for Name '{}'", accountName);
                }
            }
        }
    }

    /**
     *
     * @param data
     */
    public void storePastebin(JSONObject data) {
        try(ODatabaseDocument odb = orientDB.open(path, user, pass)) {
            odb.activateOnCurrentThread();
            String source = data.getString("Source");
            String id = data.getString("Id");
            String date = data.getString("Date");
            int count = data.getInt("EmailCount");
            odb.execute("sql", EXEC_STORE_PASTEBIN, source, id, date, count, source, id);
            LOG.debug("Successfully stored pastebin {}:{}", source, id);
        }
    }

    /**
     *
     * @param data
     */
    public void storeBreach(JSONObject data) {
        try(ODatabaseDocument odb = orientDB.open(path, user, pass)) {
            odb.activateOnCurrentThread();
            String name = data.getString("Name").replaceAll("'", "\\\\'");
            String title = data.getString("Title").replaceAll("'", "\\\\'");
            String domain = data.getString("Domain").replaceAll("'", "\\\\'");
            String breachDate = data.getString("BreachDate").replaceAll("'", "\\\\'");
            long addedDate = ZonedDateTime.parse(data.getString("AddedDate")).toInstant().toEpochMilli();
            long modifiedDate = ZonedDateTime.parse(data.getString("ModifiedDate")).toInstant().toEpochMilli();
            int pwnCount = data.getInt("PwnCount");
            String description = data.getString("Description");
            String dataClasses = data.getJSONArray("DataClasses").toString().replaceAll("'", "\\\\'");
            String logoPath = data.getString("LogoPath").replaceAll("'", "\\\\'");
            boolean isVerified = data.getBoolean("IsVerified");
            boolean isFabricated = data.getBoolean("IsFabricated");
            boolean isSensitive = data.getBoolean("IsSensitive");
            boolean isRetired = data.getBoolean("IsRetired");
            boolean isSpamList = data.getBoolean("IsSpamList");
            
            odb.execute("sql", EXEC_STORE_BREACH, name, title, domain, breachDate, 
                    addedDate, modifiedDate, pwnCount, description, dataClasses, 
                    logoPath, isVerified, isFabricated, isSensitive,  isRetired, 
                    isSpamList, title).close();
            
            LOG.debug("Successfully stored breach '{}'", title);
        }
    }

    /**
     *
     */
    public void closeDb() {
        if (db != null && !db.isClosed()) {
            db.activateOnCurrentThread();
            db.close();
        }
    }
}
