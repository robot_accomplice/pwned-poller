/*
 * Copyright (C) 2017-2019 Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.robotaccomplice.pwned;

import com.robotaccomplice.configamajig.ConfigException;
import com.robotaccomplice.configamajig.Configamajig;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public class Pwned implements Daemon {

    private final static Logger LOG = LogManager.getLogger(Pwned.class);
    private final static Options CLI_OPTIONS = new Options();

    private static String configPath;
    private static Config config;
    private static DBManager dbm;
    
    // for debugging or execute and query purposes
    private static boolean runOnce;

    // API call scheduling
    private final static Timer API_TIMER = new Timer();
    private static APIClient apiClient;
    private static ZonedDateTime startTime = ZonedDateTime.now();
    private static Frequency runFrequency = Frequency.DAILY;
    
    /**
     * Initialization method for the Commons Daemon service we won't do anything 
     * with the context as the application takes no arguments
     *
     * @param context
     * @throws DaemonInitException
     * @throws Exception
     */
    @Override
    public void init(DaemonContext context) throws DaemonInitException, Exception { }

    /**
     * Entry point for prun and jsvc service/daemon execution
     *
     * @param args
     */
    public static void start(String[] args) { main(args); }

    /**
     * Non-static start method required by Commons-Daemon
     *
     * @throws Exception
     */
    @Override
    public void start() throws Exception {
        // start timer task
        if (Pwned.runOnce) {
            LOG.info("RUN_ONCE Enabled.  Poll will execute immediately and terminate.");
            APITimerTask apiCall = new APITimerTask();
            Executors.newSingleThreadExecutor().execute(apiCall);

            // wait for poll threads to complete
            while (!apiCall.isPollDone()) {
                try { Thread.sleep(1000); } catch (InterruptedException ie) { }
            }

            try {
                stop();
            } catch (Exception e) {
                LOG.debug("Exception encountered during shutdown: {}", e.getMessage(), e);
                LOG.info("Graceful shutdown failed.  Killing process.");
                destroy();
            }
        } else {
            LOG.info("Scheduling first poll to run at {}", startTime.toLocalDateTime());
            API_TIMER.scheduleAtFixedRate(new APITimerTask(), Date.from(startTime.toInstant()), runFrequency.ms);
        }
    }

    /**
     *
     * @throws Exception
     */
    @Override
    public void stop() throws Exception { stop(null); }

    /**
     *
     * @param args
     */
    public static void stop(String[] args) {
        LOG.info("Shutdown initiated.  Cancelling API timer task...");
        API_TIMER.cancel();
        LOG.info("Closing API client...");
        apiClient.close();
        LOG.info("Shutdown complete.");
    }

    /**
     *
     */
    @Override
    public void destroy() {
        LOG.error("Destroy invoked.  Forcing system exit!");
        System.exit(ExitCodes.FORCED_SHUTDOWN.code);
    }

    /**
     *
     * @throws ConfigException
     */
    public void init() throws ConfigException {
        LOG.info("Initializing...");
        LOG.info("Initializing commandline options...");
        LOG.info("Fetching config details...");
        config = Configamajig.fromFile(Config.class, configPath);
        LOG.info("Configuration successfully initialized.  Config values are:");
        startTime = config.getStartTime();
        runFrequency = config.getFrequency();
        LOG.info("\tPolling is configured to occur {} at {} local system time.",
                runFrequency, startTime.toLocalTime());
        LOG.info("\tSyslog enabled: {}", config.isSyslogEnabled());
        LOG.info("\tDB Path       : {}", config.getDbPath());
        LOG.info("\tDB User       : {}", config.getDbUser());
        LOG.info("\tDB Password   : {}", config.getDbPass());
        LOG.info("\tAPI Url       : {}", config.getAPIUrl());
        LOG.info("\tPlease review log4j configuration file (located at: {}) for SysLog target details.",
                System.getProperty("log4j.configurationFile"));
        LOG.info("\tMonitoring the following accounts:");
        config.getAccounts().forEach((account) -> {
            LOG.info("\t\t{}", account);
        });
        LOG.info("\tMonitoring the following sites:");
        config.getSites().forEach((site) -> {
            LOG.info("\t\t{}", site);
        });

        // instantiate api client instance
        apiClient = new APIClient(config.getAPIUrl());

        // initialize data store
        LOG.info("Creating DBManager object for {}", config.getDbPath());
        dbm = new DBManager(config.getDbPath(), config.getDbUser(), config.getDbPass());
        dbm.initDB();

        LOG.info("Adding accounts to DB...");
        config.getAccounts().forEach((account) -> { dbm.storeAccount(account); });
    }
    
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        // define supported commandline options
        CLI_OPTIONS.addOption("c", "config", true, "Path to configuration file.");
        CLI_OPTIONS.addOption("r", "runonce", true, 
                "Boolean value indicating whether the poller should run once and exit or run on a schedule");
        
        // parse main args for supported commandline options
        CommandLine cli = parseCmd(args);

        // instantiate new Pwned instance
        Pwned pwned = new Pwned();

        try {
            pwned.init();
            pwned.start();
        } catch (ConfigException ce) {
            LOG.info("There was an error parsing the configuration file: {}", ce.getMessage(), ce);
            LOG.info("Shutting down...");
            try {
                pwned.stop();
            } catch (Exception e) {
                LOG.info("Unable to shutdown the daemon/service gracefully.  Forcing a hard shutdown: {}", e.getMessage(), e);
                pwned.destroy();
            }
        } catch (Exception e) {
            LOG.info("There was an unanticipated fault during startup: {}", e.getMessage(), e);
        }
    }
    
    /**
     * 
     * @param args
     * @return 
     */
    private static CommandLine parseCmd(String[] args) {
        try {
            CommandLine cli = new DefaultParser().parse(CLI_OPTIONS, args);
            
            if (!cli.hasOption('c') || cli.getOptionValue('c') == null) {
                throw new ParseException("Config file option '-c' is mandatory.");
            } else configPath = cli.getOptionValue('c');
            
            if (cli.hasOption('r')) runOnce = Boolean.parseBoolean(cli.getOptionValue('r'));
            return cli;
        } catch (ParseException pe) {
            LOG.error("Unable to parse CLI options: {}", pe.getMessage());
            showUsage("Unable to parse CLI options: " + pe.getMessage());
            System.exit(ExitCodes.BAD_OPTIONS.code);
            return null;
        }
    }
    
    /**
     * 
     * @param message 
     */
    private static void showUsage(String message) {
        System.out.println(message);
        new HelpFormatter().printHelp("Commandline Help: ", CLI_OPTIONS);
    }

    /**
     * @class APITimerTask @extends TimerTask
     *
     * Invokes the APIClient at the configuration specified time
     */
    private class APITimerTask extends TimerTask {

        // process completion flags
        private volatile boolean accountDone = false;
        private volatile boolean pasteDone = false;
        private volatile boolean siteDone = false;
        private volatile boolean pollDone = false;

        private ZonedDateTime pollStart;

        Map<String, Integer> accountBreachStats = new HashMap<>();
        Map<String, Integer> pastebinStats = new HashMap<>();
        Map<String, Integer> siteBreachStats = new HashMap<>(); 
            
        /**
         * single-threaded account breach processing (runs in own thread to maximize parallelism with pastebin and site
         * breach checks)
         */
        private void processAccountBreaches() {
            Executors.newSingleThreadExecutor().execute(() -> {
                config.getAccounts().forEach(account -> {
                    accountBreachStats.put(account, 0);
                    Optional<String> breach = apiClient.getAccountBreach(account);
                    if (breach.isPresent()) {
                        String breachData = breach.get();
                        LOG.debug("Account \"{}\" has the following breaches:\n{}", account, breachData);

                        try {
                            if(breachData == null || breachData.isEmpty()) {
                                LOG.info("Breach data was empty.");
                            } else {
                                new JSONArray(breachData).forEach((o) -> {
                                    try {
                                        if (o instanceof JSONObject) {
                                            accountBreachStats.put(account, accountBreachStats.get(account) + 1);
                                            JSONObject data = (JSONObject) o;

                                            dbm.storeBreach(data);
                                            // add relationship between account and breach
                                            dbm.addAccountBreach(account, data.getString("Name"));

                                            data.putOnce("Account", account); // add account as context
                                            if(config.isSyslogEnabled()) {
                                                LOG.info("SYSLOG - Sending account breach for account {} to syslog server",
                                                        account);
                                                SysLogger.log(Level.INFO, data.toString()); // send syslog message
                                            }
                                        }
                                    } catch (JSONException je) {
                                        LOG.info("Unanticipated fault while processing JSONObject of specific account breach data: {}",
                                                je.getMessage(), je);
                                    }
                                });
                            }
                        } catch (JSONException je) {
                            LOG.info("Unanticipated fault while processing JSONArray of account breach data: {}",
                                    je.getMessage(), je);
                        }
                    } else {
                        LOG.info("Account \"{}\" was not present in the breach results", account);
                    }

                    try {
                        // sleep for API spec rate throttling period + 100 ms (as suggested by API documentation)
                        Thread.sleep(1600);
                    } catch (InterruptedException ie) {
                        // do nothing
                    }
                });

                LOG.info("Account breach data collection complete.");
                // if all data collection is complete, end the poll otherwise, set the account done flag
                accountDone = true;
                endPoll();
            });
        }

        /**
         * single-threaded account breach processing (runs in own thread to maximize parallelism with account and site
         * breach checks)
         */
        private void processPastebin() {
            Executors.newSingleThreadExecutor().execute(() -> {
                config.getAccounts().forEach(account -> {
                    pastebinStats.put(account, 0);
                    Optional<String> pastebin = apiClient.getAccountPastebin(account);
                    if (pastebin.isPresent()) {
                        String pastebinData = pastebin.get();

                        LOG.debug("Account \"{}\" has the following pastebin hits:\n{}", account, pastebinData);

                        try {
                            JSONArray jArray = new JSONArray(pastebinData);
                            jArray.forEach((o) -> {
                                if (o instanceof JSONObject) {
                                    pastebinStats.put(account, 
                                            pastebinStats.get(account) + 1);
                                    JSONObject data = (JSONObject) o;

                                    dbm.storePastebin(data);
                                    // store associations with account
                                    dbm.addAccountPastebin(account, data.getString("Source"), data.getString("Id"));

                                    data.putOnce("Account", account); // add account as context
                                    if(config.isSyslogEnabled()) {
                                        LOG.info("SYSLOG - Sending Pastebin hit for account {} to syslog server", account);
                                        SysLogger.log(Level.INFO, data.toString()); // send syslog message
                                    } else {
                                        LOG.info("Found Pastebin hit for account {}", account);
                                    }
                                }
                            });
                        } catch (JSONException je) {
                            LOG.info(je.getMessage(), je);
                        }

                    } else {
                        LOG.info("Account \"{}\" was not present in the pastebin results", account);
                    }
                    
                    try {
                        // sleep for API spec rate throttling period + 100 ms (as suggested by API documentation)
                        Thread.sleep(1600);
                    } catch (InterruptedException ie) {
                        // do nothing 
                    }
                });

                LOG.info("Account pastebin data collection complete.");
                // if all data collection is complete, end the poll otherwise, set the pastebin done flag
                pasteDone = true;
                endPoll();
            });
        }

        /**
         * single-threaded account breach processing (runs in own thread to maximize parallelism with pastebin and
         * account breach checks)
         */
        private void processSiteBreaches() {
            config.getSites().forEach((site) -> { siteBreachStats.put(site, 0); });
            LOG.info("Fetching site breaches...");
            Optional<String> allBreaches = apiClient.getAllBreaches();
            if (allBreaches.isPresent()) {
                String allBreachesData = allBreaches.get();

                LOG.debug("Retrieved the following \"all breaches\" data\n{}", allBreachesData);

                try {
                    LOG.info("Processing site breaches...");
                    JSONArray jArray = new JSONArray(allBreachesData);

                    jArray.forEach((o) -> {
                        if (o instanceof JSONObject) {
                            JSONObject data = (JSONObject) o;


                            LOG.debug("Storing breach data for '{}'", data.getString("Title"));
                            dbm.storeBreach(data);

                            // if this breach entry contains a domain on our list, submit a syslog entry
                            if (data.has("Domain") && !data.isNull("Domain")) {
                                String breachDomain = data.getString("Domain");
                                config.getSites().forEach((site) -> {
                                    if (breachDomain.toLowerCase().contains(site.toLowerCase())) {
                                        siteBreachStats.put(site, siteBreachStats.get(site) + 1);
                                        if(config.isSyslogEnabled()) {
                                            LOG.info("SYSLOG - Sending site breach for domain '{}' to syslog server",
                                                    breachDomain);
                                            SysLogger.log(Level.INFO, data.toString()); // send syslog message
                                        } else {
                                            LOG.info("Identified site breach for doamin '{}'", site);
                                        }
                                    } else {
                                        LOG.trace("Domain \"{}\" does not contain \"{}\"", breachDomain, site);
                                    }
                                });
                            } else {
                                LOG.info("Breach '{}' of {} breaches does not contain a Domain field.",
                                        data.getString("Title"), config.getSites().size());
                            }
                        }
                    });
                } catch (JSONException e) {
                    LOG.info(e.getMessage(), e);
                }
            } else {
                LOG.info("We failed to retrieve the site breach data.  Please review the APIClient log entries for more detail.");
            }

            LOG.info("Site breach data collection complete.");
            // if all data collection is complete, end the poll
            // otherwise, set the account done flag
            siteDone = true;
            endPoll();
        }

        /**
         * end of poll cleanup
         */
        private synchronized void endPoll() {
            if(accountDone && siteDone && pasteDone) {
                // reset completion flags in preparation for next poll
                accountDone = false;
                siteDone = false;
                pasteDone = false;
                pollDone = true;

                if(runOnce) {
                    LOG.info("Poll complete.  Invoking stop()");
                    stop(null);
                } else {
                    LOG.info("Poll complete.  Next poll will occur at {}",
                        pollStart.plusSeconds(runFrequency.ms / 1000));
                }
                dbm.closeDb();

                showSummary();
            } else {
                if(!accountDone) LOG.info("Still waiting on Account polling...");
                if(!siteDone) LOG.info("Still waiting on Site polling...");
                if(!pasteDone) LOG.info("Still waiting on Pastebin polling...");
            }
        }

        /**
         * 
         */
        private void showSummary() {
            LOG.info("Account Breaches Found:");
            accountBreachStats.forEach((account, count) -> { LOG.info("\t{} = {}", account, count); });
            accountBreachStats.clear();

            LOG.info("Pastebin Entries Found:");
            pastebinStats.forEach((account, count) -> { LOG.info("\t{} = {}", account, count); });
            pastebinStats.clear();

            LOG.info("Site Breaches Found:");
            siteBreachStats.forEach((site, count) -> { LOG.info("\t{} = {}", site, count); });
            siteBreachStats.clear();
        }

        /**
         *
         */
        @Override
        public void run() {
            pollDone = false;
            pollStart = ZonedDateTime.now();
            LOG.info("Polling \"{}\":", config.getAPIUrl());

            LOG.info("Retrieving account breach data...");
            processAccountBreaches();

            LOG.info("Retrieving account pastebin data...");
            processPastebin();

            LOG.info("Retrieving site breach data...");
            processSiteBreaches();
        }

        /**
         * 
         * @return a boolean value indicating whether or not the poll is finished
         */
        private boolean isPollDone() {
            return pollDone;
        }
    }
}
