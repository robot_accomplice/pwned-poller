/*
 * Copyright (C) 2017-2019 Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.robotaccomplice.pwned;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.robotaccomplice.configamajig.ConfigException;
import com.robotaccomplice.configamajig.Configamajig;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
@JsonIgnoreProperties({"valid", "apiurl"})
public class Config implements Configamajig {

    private final static String API_URL = "https://haveibeenpwned.com/api/v2";

    /**
     * must be ISO_LOCAL_TIME parseable @see java.time.format.DateTimeFormatter.ISO_LOCAL_TIME default value is midnight
     * (00:00)
     */
    @JsonProperty("startTime")
    private String startTime = "00:00";

    @JsonProperty("sites")
    private List<String> sites;

    @JsonProperty("accounts")
    private List<String> accounts;

    @JsonProperty("dbPath")
    private String dbPath;

    @JsonProperty("dbUser")
    private String dbUser;

    @JsonProperty("dbPass")
    private String dbPass;
    
    @JsonProperty("frequency")
    private Frequency frequency = Frequency.DAILY;

    @JsonProperty("syslogEnabled")
    private boolean syslogEnabled;

    @JsonProperty("runOnce")
    private boolean runOnce;

    /**
     *
     * @return
     */
    String getAPIUrl() { return API_URL; }

    /**
     * parses String representation of "startTime" and produces a UTC zoned ZonedDateTime based on the next occurrence
     * of the specified time Config file value must be ISO_LOCAL_TIME parseable @see
     * java.time.format.DateTimeFormatter.ISO_LOCAL_TIME
     *
     * @return
     * @throws ConfigException
     */
    ZonedDateTime getStartTime() throws ConfigException {
        try {
            ZonedDateTime zdt = ZonedDateTime.of(LocalDate.now(), LocalTime.parse(startTime), ZoneOffset.systemDefault());

            if (zdt.isBefore(ZonedDateTime.now())) return zdt.plusDays(1);
            else return zdt;
        } catch (DateTimeParseException dtpe) {
            throw new ConfigException("Bad config:  startTime \"" + startTime + "\" is unparseable!", dtpe);
        }
    }

    /**
     *
     * @return
     */
    List<String> getSites() { return sites; }

    /**
     *
     * @return
     */
    List<String> getAccounts() { return accounts; }

    /**
     *
     * @return
     */
    String getDbPath() { return dbPath; }

    /**
     *
     * @return
     */
    String getDbUser() { return dbUser; }

    /**
     *
     * @return
     */
    public String getDbPass() { return dbPass; }
    
    /**
     * 
     * @return 
     */
    public Frequency getFrequency() { return frequency; }

    /**
     *
     * @return
     */
    public boolean isSyslogEnabled() { return syslogEnabled; }

    /**
     *
     * @return
     */
    public boolean isRunOnce() { return runOnce; }

    /**
     *
     * @return @throws ConfigException
     */
    @Override
    public boolean isValid() throws ConfigException { return true; }
}
