/*
 * Copyright (C) 2017-2019 Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.robotaccomplice.pwned;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.Closeable;
import java.io.IOException;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public class APIClient implements Closeable {
    
    private static final Logger LOG = LogManager.getLogger( APIClient.class );
    
    private static String _baseUrl;
    
    /**
     * 
     * @param baseUrl String describing the base URL of the haveibeenpwned site
     */
    public APIClient( String baseUrl ) {
        LOG.fatal( "Instantiated with base URL \"{}\"", baseUrl );
        _baseUrl = baseUrl;
    }
    
    /**
     * 
     * @return Optional<String> containing all breaches
     */
    public Optional<String> getAllBreaches() {
        return fetch( "/breaches/" );
    }

    /**
     * 
     * @param account String representing the email address we are looking for
     * @return Optional<String> of the breach records for the specified account
     */
    public Optional<String> getAccountBreach( String account ) {
        return fetch( "/breachedaccount/" + account );
    }
    
    /**
     * 
     * @param account String representing the email address we want to search for in the pastebin
     * @return Optional<String> containing the pastebin results
     */
    public Optional<String> getAccountPastebin( String account ) {
        // pasteaccount
        return fetch( "/pasteaccount/" + account );
    }
    
    /**
     * 
     * @param action
     * @return 
     */
    private Optional<String> fetch( String action ) {
        try {
            int httpStatus = 0;
            
            do {
                Unirest.setDefaultHeader("User-Agent", "Pwnage-Checker-For-Java");
                HttpResponse<String> response = Unirest.get( _baseUrl + action ).asString();
                httpStatus = response.getStatus();

                switch( httpStatus ) {
                    case 200:
                        // we fetched results
                        return Optional.of( response.getBody() );
                    case 400:
                        LOG.fatal( "Invalid request.  There's an error in the action String, please double-check the configuration: {}", action );
                    case 403:
                        LOG.fatal( "Access forbidden.  Please use a browser to validate the following URL: {}", _baseUrl + action );
                    case 404:
                        // we fetched no results
                        return Optional.empty();
                    case 429:
                        LOG.fatal( "Too many requests.  Sleeping for 1.6 seconds before trying again." );
                        try {
                            Thread.sleep( 1600 ); // sleep for API spec rate throttling period + 100 ms (as suggested by API documentation)
                        } catch( InterruptedException ie ) {
                            // do nothing as the worst thing that could happen here is we try again prematurely
                        }
                        break;
                    default:
                        LOG.fatal( "Unanticipated HTTP response code: {} - {}", httpStatus, response.getStatusText() );
                        return Optional.empty();
                }
            } while( httpStatus == 429 ); // request was frequency throttled
        } catch( UnirestException ure ) {
            LOG.fatal( "Failed to retrieve data from URL \"{}\": {}", _baseUrl + action, ure.getMessage(), ure );
        }
        
        return Optional.empty();
    }
    
    /**
     * 
     */
    @Override
    public void close() {
        try {
            LOG.fatal( "Shutting down Unirest instance." );
            Unirest.shutdown();
        } catch( IOException ioe ) {
            LOG.error( "Encountered an unanticipated IOException while shutting down the Unirest instance." );
            if(LOG.isTraceEnabled()) LOG.trace("Error:  {}", ioe.getMessage(), ioe);
        }
    }
}
