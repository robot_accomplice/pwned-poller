/*
 * Copyright (C) 2017 Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.robotaccomplice.pwned;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jonathan Machen {@literal <jon.machen@robotaccomplice.com>}
 */
public class SysLogger {

    private final static Logger LOG = LogManager.getLogger(SysLogger.class);

    /**
     *
     * @param message
     * @param level
     * @param objs
     */
    public static void log(String message, Level level, Object... objs) {
        LOG.log(Level.FATAL, message, objs);
    }

    /**
     *
     * @param level
     * @param message
     */
    public static void log(Level level, String message) {
        LOG.log(level, message);
    }
}
